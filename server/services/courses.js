/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     courses.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/server/service.js.vm
 */
"use strict";

let db = require('./pghelper');

let findAll = (req, res, next) => {
    let periodId = req.query.periodId;
    let sql = `
        SELECT  c.code , c.credits , c.enrollments , c.id , c.name , c.period , c.teacher        FROM course as c
                                                                                                                                ;
    db.query(sql, [])
        .then(result => res.json(result))
        .catch(next);
};

let findByPeriod = (req, res, next) => {
    let periodId = req.params.id;
        let sql = `
    SELECT c.id, c.name FROM period as c;
    db.query(sql, [parseInt(teacherId)])
        .then(courses => res.json(courses))
        .catch(next);
};
let findByTeacher = (req, res, next) => {
        let teacherId = req.params.id;
        let sql = `
        SELECT  c.address , c.city , c.courses , c.email , c.firstName , c.id , c.lastName , c.mobilePhone , c.phone , c.pic , c.state , c.title , c.zip        FROM teacher as c;
    db.query(sql, [parseInt(teacherId)])
        .then(courses =>  res.json(courses))
        .catch(next);
};



let findById = (req, res, next) => {
    let id = req.params.id;
        let sql = `
        SELECT c.code, c.credits, c.enrollments, c.id, c.name, c.period, c.teacher FROM course as c;
        WHERE c.id = $1 `;
    db.query(sql, [parseInt(id)])
        .then(courses =>  res.json(courses[0]))
        .catch(next);
};

let createItem = (req, res, next) => {
    let course = req.body;
        let sql = `
        INSERT INTO course(code, credits, enrollments, id, name, period, teacher)
        VALUES($0, $1, $2, $3, $4, $5, $6)
        `;
    db.query(sql, [ , course.code , course.credits , course.enrollments , course.id , course.name , course.period , course.teacher])
        .then(result => res.send({id: result.insertId}))
        .catch(next);
};

let updateItem = (req, res, next) => {
    let course = req.body;
        let sql = `
        UPDATE course SET code = $0, credits = $1, enrollments = $2, id = $3, name = $4, period = $5, teacher = $6 WHERE id = $7 `;
        db.query(sql, [course.code ,course.credits ,course.enrollments ,course.id ,course.name ,course.period ,course.teacher])
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

let deleteItem = (req, res, next) => {
    let courseId = req.params.id;
    db.query('DELETE FROM course WHERE id=$1', [courseId], true)
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

exports.findAll = findAll;
exports.findByPeriod = findByPeriod;
exports.findByTeacher = findByTeacher;

exports.findById = findById;
exports.createItem = createItem;
exports.updateItem = updateItem;
exports.deleteItem = deleteItem;

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 28.83 minutes to type the 2883+ characters in this file.
 */