/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     students.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/server/service.js.vm
 */
"use strict";

let db = require('./pghelper');

let findAll = (req, res, next) => {
        let periodId = req.query.periodId;
        let sql = `
        SELECT  c.address , c.city , c.dob , c.email , c.enrollments , c.firstName , c.id , c.lastName , c.lastupdate , c.mobilePhone , c.phone , c.pic , c.registration , c.state , c.zip        FROM student as c
                                                                                                                                                                                                                                                                ;
    db.query(sql, [])
        .then(result => res.json(result))
        .catch(next);
};




let findById = (req, res, next) => {
    let id = req.params.id;
        let sql = `
        SELECT c.address, c.city, c.dob, c.email, c.enrollments, c.firstName, c.id, c.lastName, c.lastupdate, c.mobilePhone, c.phone, c.pic, c.registration, c.state, c.zip FROM student as c;
        WHERE c.id = $1 `;
    db.query(sql, [parseInt(id)])
        .then(students =>  res.json(students[0]))
        .catch(next);
};

let createItem = (req, res, next) => {
    let student = req.body;
        let sql = `
        INSERT INTO student(address, city, dob, email, enrollments, firstName, id, lastName, lastupdate, mobilePhone, phone, pic, registration, state, zip)
        VALUES($0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
        `;
    db.query(sql, [ , student.address , student.city , student.dob , student.email , student.enrollments , student.firstName , student.id , student.lastName , student.lastupdate , student.mobilePhone , student.phone , student.pic , student.registration , student.state , student.zip])
        .then(result => res.send({id: result.insertId}))
        .catch(next);
};

let updateItem = (req, res, next) => {
    let student = req.body;
        let sql = `
        UPDATE student SET address = $0, city = $1, dob = $2, email = $3, enrollments = $4, firstName = $5, id = $6, lastName = $7, lastupdate = $8, mobilePhone = $9, phone = $10, pic = $11, registration = $12, state = $13, zip = $14 WHERE id = $15 `;
        db.query(sql, [student.address ,student.city ,student.dob ,student.email ,student.enrollments ,student.firstName ,student.id ,student.lastName ,student.lastupdate ,student.mobilePhone ,student.phone ,student.pic ,student.registration ,student.state ,student.zip])
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

let deleteItem = (req, res, next) => {
    let studentId = req.params.id;
    db.query('DELETE FROM student WHERE id=$1', [studentId], true)
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

exports.findAll = findAll;

exports.findById = findById;
exports.createItem = createItem;
exports.updateItem = updateItem;
exports.deleteItem = deleteItem;

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 31.689999 minutes to type the 3169+ characters in this file.
 */