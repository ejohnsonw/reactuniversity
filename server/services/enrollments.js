/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     enrollments.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/server/service.js.vm
 */
"use strict";

let db = require('./pghelper');

let findAll = (req, res, next) => {
    let periodId = req.query.periodId;
    let sql = `
        SELECT  c.course , c.id , c.student        FROM enrollment as c
                                                                ;
    db.query(sql, [])
        .then(result => res.json(result))
        .catch(next);
};

let findByCourse = (req, res, next) => {
    let courseId = req.params.id;
        let sql = `
    SELECT c.code, c.credits, c.enrollments, c.id, c.name, c.period, c.teacher FROM course as c;
    db.query(sql, [parseInt(teacherId)])
        .then(enrollments => res.json(enrollments))
        .catch(next);
};
let findByStudent = (req, res, next) => {
        let studentId = req.params.id;
        let sql = `
        SELECT  c.address , c.city , c.dob , c.email , c.enrollments , c.firstName , c.id , c.lastName , c.lastupdate , c.mobilePhone , c.phone , c.pic , c.registration , c.state , c.zip        FROM student as c;
    db.query(sql, [parseInt(teacherId)])
        .then(enrollments =>  res.json(enrollments))
        .catch(next);
};



let findById = (req, res, next) => {
    let id = req.params.id;
        let sql = `
        SELECT c.course, c.id, c.student FROM enrollment as c;
        WHERE c.id = $1 `;
    db.query(sql, [parseInt(id)])
        .then(enrollments =>  res.json(enrollments[0]))
        .catch(next);
};

let createItem = (req, res, next) => {
    let enrollment = req.body;
        let sql = `
        INSERT INTO enrollment(course, id, student)
        VALUES($0, $1, $2)
        `;
    db.query(sql, [ , enrollment.course , enrollment.id , enrollment.student])
        .then(result => res.send({id: result.insertId}))
        .catch(next);
};

let updateItem = (req, res, next) => {
    let enrollment = req.body;
        let sql = `
        UPDATE enrollment SET course = $0, id = $1, student = $2 WHERE id = $3 `;
        db.query(sql, [enrollment.course ,enrollment.id ,enrollment.student])
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

let deleteItem = (req, res, next) => {
    let enrollmentId = req.params.id;
    db.query('DELETE FROM enrollment WHERE id=$1', [enrollmentId], true)
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

exports.findAll = findAll;
exports.findByCourse = findByCourse;
exports.findByStudent = findByStudent;

exports.findById = findById;
exports.createItem = createItem;
exports.updateItem = updateItem;
exports.deleteItem = deleteItem;

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 25.97 minutes to type the 2597+ characters in this file.
 */