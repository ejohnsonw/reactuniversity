/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     teachers.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/server/service.js.vm
 */
"use strict";

let db = require('./pghelper');

let findAll = (req, res, next) => {
        let periodId = req.query.periodId;
        let sql = `
        SELECT  c.address , c.city , c.courses , c.email , c.firstName , c.id , c.lastName , c.mobilePhone , c.phone , c.pic , c.state , c.title , c.zip        FROM teacher as c
                                                                                                                                                                                                                                ;
    db.query(sql, [])
        .then(result => res.json(result))
        .catch(next);
};




let findById = (req, res, next) => {
    let id = req.params.id;
        let sql = `
        SELECT c.address, c.city, c.courses, c.email, c.firstName, c.id, c.lastName, c.mobilePhone, c.phone, c.pic, c.state, c.title, c.zip FROM teacher as c;
        WHERE c.id = $1 `;
    db.query(sql, [parseInt(id)])
        .then(teachers =>  res.json(teachers[0]))
        .catch(next);
};

let createItem = (req, res, next) => {
    let teacher = req.body;
        let sql = `
        INSERT INTO teacher(address, city, courses, email, firstName, id, lastName, mobilePhone, phone, pic, state, title, zip)
        VALUES($0, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
        `;
    db.query(sql, [ , teacher.address , teacher.city , teacher.courses , teacher.email , teacher.firstName , teacher.id , teacher.lastName , teacher.mobilePhone , teacher.phone , teacher.pic , teacher.state , teacher.title , teacher.zip])
        .then(result => res.send({id: result.insertId}))
        .catch(next);
};

let updateItem = (req, res, next) => {
    let teacher = req.body;
        let sql = `
        UPDATE teacher SET address = $0, city = $1, courses = $2, email = $3, firstName = $4, id = $5, lastName = $6, mobilePhone = $7, phone = $8, pic = $9, state = $10, title = $11, zip = $12 WHERE id = $13 `;
        db.query(sql, [teacher.address ,teacher.city ,teacher.courses ,teacher.email ,teacher.firstName ,teacher.id ,teacher.lastName ,teacher.mobilePhone ,teacher.phone ,teacher.pic ,teacher.state ,teacher.title ,teacher.zip])
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

let deleteItem = (req, res, next) => {
    let teacherId = req.params.id;
    db.query('DELETE FROM teacher WHERE id=$1', [teacherId], true)
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

exports.findAll = findAll;

exports.findById = findById;
exports.createItem = createItem;
exports.updateItem = updateItem;
exports.deleteItem = deleteItem;

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 28.63 minutes to type the 2863+ characters in this file.
 */