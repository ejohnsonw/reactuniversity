/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     periods.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/server/service.js.vm
 */
"use strict";

let db = require('./pghelper');

let findAll = (req, res, next) => {
        let periodId = req.query.periodId;
        let sql = `
        SELECT  c.id , c.name        FROM period as c
                                                ;
    db.query(sql, [])
        .then(result => res.json(result))
        .catch(next);
};




let findById = (req, res, next) => {
    let id = req.params.id;
        let sql = `
        SELECT c.id, c.name FROM period as c;
        WHERE c.id = $1 `;
    db.query(sql, [parseInt(id)])
        .then(periods =>  res.json(periods[0]))
        .catch(next);
};

let createItem = (req, res, next) => {
    let period = req.body;
        let sql = `
        INSERT INTO period(id, name)
        VALUES($0, $1)
        `;
    db.query(sql, [ , period.id , period.name])
        .then(result => res.send({id: result.insertId}))
        .catch(next);
};

let updateItem = (req, res, next) => {
    let period = req.body;
        let sql = `
        UPDATE period SET id = $0, name = $1 WHERE id = $2 `;
        db.query(sql, [period.id ,period.name])
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

let deleteItem = (req, res, next) => {
    let periodId = req.params.id;
    db.query('DELETE FROM period WHERE id=$1', [periodId], true)
        .then(() => res.send({result: 'ok'}))
        .catch(next);
};

exports.findAll = findAll;

exports.findById = findById;
exports.createItem = createItem;
exports.updateItem = updateItem;
exports.deleteItem = deleteItem;

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 15.64 minutes to type the 1564+ characters in this file.
 */