/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     TeacherEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /ionic-1.6.1/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:TeachersCtrl
 * @description
 * # TeachersCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('TeacherEditCtrl', function($scope, $stateParams, $rootScope, CourseWS, TeacherWS) {
        $scope.teacher = {};




        $scope.edit = true;
        $scope.save = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.teacher;
            } else {
                data = $scope.teacher;
            }
            TeacherWS.update($scope.updatesuccesscb, $scope.errorcb, data);
        }

        $scope.delete = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.teacher;
            } else {
                data = $scope.teacher;
            }
            TeacherWS.delete($scope.updatesuccesscb, $scope.errorcb, data);
        }
        $scope.cancel = function() {
            document.location.href = "#/teachers"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/teachers"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.teacher = data;

        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }
        $scope.loadReferences = function() {

            // false String String address
            // false String String city
            // true Collection Course courses
            CourseWS.list(function(data) {
                    $scope.coursess = data;
                }, $scope.errorcb)
                // false String String email
                // false String String firstName
                // false String String id
                // false String String lastName
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false String String state
                // false String String title
                // false String String zip

        }
        $scope.loadReferences();
        TeacherWS.retrieve($scope.successcb, $scope.errorcb, $stateParams.id);
    }).config(function($stateProvider) {
        $stateProvider
            .state('app.editTeacher', {
                url: '/teacher/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/teacher.html',
                        controller: 'TeacherEditCtrl'
                    }
                }
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 25.65 minutes to type the 2565+ characters in this file.
 */