/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     StudentEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /ionic-1.6.1/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:StudentsCtrl
 * @description
 * # StudentsCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('StudentEditCtrl', function($scope, $stateParams, $rootScope, StudentWS, EnrollmentWS) {
        $scope.student = {};


        var dobDatePickerCallBack = function(val) {
            if (typeof(val) === 'undefined') {
                console.log('No date selected');
            } else {
                console.log('Selected date is : ', val)
            }
        };

        $scope.dobDatePicker = {
            titleLabel: 'Dob', //Optional
            todayLabel: 'Today', //Optional
            closeLabel: 'Close', //Optional
            setLabel: 'Set', //Optional
            setButtonType: 'button-assertive', //Optional
            todayButtonType: 'button-assertive', //Optional
            closeButtonType: 'button-assertive', //Optional
            mondayFirst: true, //Optional
            templateType: 'popup', //Optional
            showTodayButton: 'true', //Optional
            modalHeaderColor: 'bar-positive', //Optional
            modalFooterColor: 'bar-positive', //Optional
            callback: function(val) { //Mandatory
                dobDatePickerCallBack(val);
            }
        };



        $scope.edit = true;
        $scope.save = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.student;
            } else {
                data = $scope.student;
            }
            StudentWS.update($scope.updatesuccesscb, $scope.errorcb, data);
        }

        $scope.delete = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.student;
            } else {
                data = $scope.student;
            }
            StudentWS.delete($scope.updatesuccesscb, $scope.errorcb, data);
        }
        $scope.cancel = function() {
            document.location.href = "#/students"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/students"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.student = data;
            $scope.dobDatePicker.inputDate = new Date(data.dob);

        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }
        $scope.loadReferences = function() {

            // false String String address
            // false String String city
            // false Date String dob
            // false String String email
            // true Collection Enrollment enrollments
            EnrollmentWS.list(function(data) {
                    $scope.enrollmentss = data;
                }, $scope.errorcb)
                // false String String firstName
                // false String String id
                // false String String lastName
                // false timestamp String lastupdate
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false timestamp String registration
                // false String String state
                // false String String zip

        }
        $scope.loadReferences();
        StudentWS.retrieve($scope.successcb, $scope.errorcb, $stateParams.id);
    }).config(function($stateProvider) {
        $stateProvider
            .state('app.editStudent', {
                url: '/student/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/student.html',
                        controller: 'StudentEditCtrl'
                    }
                }
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 36.32 minutes to type the 3632+ characters in this file.
 */