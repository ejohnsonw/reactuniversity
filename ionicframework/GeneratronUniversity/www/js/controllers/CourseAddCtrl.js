/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CourseAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      GeneratronUniversity
Template: /ionic-1.6.1/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:CourseAddCtrl
 * @description
 * # CourseAddCtrl
 * Controller of the GeneratronUniversity
 */
angular.module('GeneratronUniversity.controllers')
    .controller('CourseAddCtrl', function($scope, PeriodWS, TeacherWS, CourseWS, EnrollmentWS) {

        $scope.course = {};

        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/app/courses"
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            var data = {};

            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.course;
            } else {
                data = $scope.course;
            }

            CourseWS.create($scope.successcb, $scope.errorcb, data);
        }

        $scope.cancel = function() {
            document.location.href = "#/courses"
        }

        $scope.loadReferences = function() {
            // false String String code
            // false Integer String credits
            // true Collection Enrollment enrollments
            EnrollmentWS.list(function(data) {
                    $scope.enrollmentss = data;
                }, $scope.errorcb)
                // false String String id
                // false String String name
                // false Period String period
            PeriodWS.list(function(data) {
                    $scope.periods = data;
                }, $scope.errorcb)
                // false Teacher String teacher
            TeacherWS.list(function(data) {
                $scope.teachers = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();


    }).config(function($stateProvider) {
        $stateProvider
            .state('app.addCourse', {
                url: '/course',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/course.html',
                        controller: 'CourseAddCtrl'
                    }
                }
            })

    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 22.34 minutes to type the 2234+ characters in this file.
 */