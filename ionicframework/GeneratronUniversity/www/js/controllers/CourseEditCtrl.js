/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CourseEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /ionic-1.6.1/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:CoursesCtrl
 * @description
 * # CoursesCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('CourseEditCtrl', function($scope, $stateParams, $rootScope, PeriodWS, TeacherWS, CourseWS, EnrollmentWS) {
        $scope.course = {};




        $scope.edit = true;
        $scope.save = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.course;
            } else {
                data = $scope.course;
            }
            CourseWS.update($scope.updatesuccesscb, $scope.errorcb, data);
        }

        $scope.delete = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.course;
            } else {
                data = $scope.course;
            }
            CourseWS.delete($scope.updatesuccesscb, $scope.errorcb, data);
        }
        $scope.cancel = function() {
            document.location.href = "#/courses"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/courses"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.course = data;

        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }
        $scope.loadReferences = function() {

            // false String String code
            // false Integer String credits
            // true Collection Enrollment enrollments
            EnrollmentWS.list(function(data) {
                    $scope.enrollmentss = data;
                }, $scope.errorcb)
                // false String String id
                // false String String name
                // false Period String period
            PeriodWS.list(function(data) {
                    $scope.periods = data;
                }, $scope.errorcb)
                // false Teacher String teacher
            TeacherWS.list(function(data) {
                $scope.teachers = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();
        CourseWS.retrieve($scope.successcb, $scope.errorcb, $stateParams.id);
    }).config(function($stateProvider) {
        $stateProvider
            .state('app.editCourse', {
                url: '/course/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/course.html',
                        controller: 'CourseEditCtrl'
                    }
                }
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 24.78 minutes to type the 2478+ characters in this file.
 */