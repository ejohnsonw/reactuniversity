
#!/bin/bash		      
spring stop
rails generate controller welcome index
rails generate scaffold Period${entity.rubyDeclaration(${dth})}
rails generate scaffold Teacher${entity.rubyDeclaration(${dth})}
rails generate scaffold Course${entity.rubyDeclaration(${dth})}
rails generate scaffold Student${entity.rubyDeclaration(${dth})}
rails generate scaffold Enrollment${entity.rubyDeclaration(${dth})}

bundle install

#rake db:drop
#rake db:create
#rake db:migrate
