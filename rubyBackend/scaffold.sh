
#!/bin/bash		      
spring stop
rails generate controller welcome index
rails generate scaffold Period name:String
rails generate scaffold Teacher address:String city:String email:String firstName:String lastName:String mobilePhone:String phone:String pic:String state:String title:String zip:String
rails generate scaffold Course code:String credits:Integer name:String period:belongs_to teacher:belongs_to
rails generate scaffold Student address:String city:String dob:Date email:String firstName:String lastName:String lastupdate:timestamp mobilePhone:String phone:String pic:String registration:timestamp state:String zip:String
rails generate scaffold Enrollment course:belongs_to student:belongs_to

bundle install

#rake db:drop
#rake db:create
#rake db:migrate
