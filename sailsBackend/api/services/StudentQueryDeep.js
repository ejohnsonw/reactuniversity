/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     StudentQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      GeneratronUniversity
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var StudentPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                student: function(cb) {
                    Student.findOne({
                        where: {
                            id: id
                        }
                    }).populate("enrollments").exec(cb);
                }


                //Relations for enrollments
                ,
                coursesForenrollments: ['student',
                    function(cb, results) {
                        Course.find({
                            id: _.pluck(results.student.enrollments, 'course')
                        }).exec(cb);
                    }
                ],
                enrollmentsDeep: ['coursesForenrollments',
                    function(cb, results) {

                        var courses = _.indexBy(results.coursesForenrollments, 'id');

                        var student = results.student.toObject();
                        student.enrollmentsMapped = _.map(student.enrollments, function(enrollment) {
                            enrollment.course = courses[enrollment.course];
                            return enrollment;
                        });

                        return cb(null, student);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = StudentPopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 14.97 minutes to type the 1497+ characters in this file.
 */