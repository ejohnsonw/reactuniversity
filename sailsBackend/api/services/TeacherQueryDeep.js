/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     TeacherQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      GeneratronUniversity
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var TeacherPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                teacher: function(cb) {
                    Teacher.findOne({
                        where: {
                            id: id
                        }
                    }).populate("courses").exec(cb);
                }


                //Relations for courses
                ,
                enrollmentssForcourses: ['teacher',
                    function(cb, results) {
                        Enrollment.find({
                            id: _.pluck(results.teacher.courses, 'enrollments')
                        }).exec(cb);
                    }
                ],
                periodsForcourses: ['teacher',
                    function(cb, results) {
                        Period.find({
                            id: _.pluck(results.teacher.courses, 'period')
                        }).exec(cb);
                    }
                ],
                coursesDeep: ['enrollmentssForcourses', 'periodsForcourses',
                    function(cb, results) {

                        var enrollmentss = _.indexBy(results.enrollmentssForcourses, 'id');
                        var periods = _.indexBy(results.periodsForcourses, 'id');

                        var teacher = results.teacher.toObject();
                        teacher.coursesMapped = _.map(teacher.courses, function(course) {
                            course.enrollments = enrollmentss[course.enrollments];
                            course.period = periods[course.period];
                            return course;
                        });

                        return cb(null, teacher);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = TeacherPopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 19.35 minutes to type the 1935+ characters in this file.
 */