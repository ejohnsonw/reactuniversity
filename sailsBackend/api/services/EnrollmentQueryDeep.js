/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     EnrollmentQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      GeneratronUniversity
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var EnrollmentPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                enrollment: function(cb) {
                    Enrollment.findOne({
                        where: {
                            id: id
                        }
                    }).populate("course").populate("student").exec(cb);
                }


                //Relations for course
                ,
                teachersForcourse: ['enrollment',
                    function(cb, results) {
                        Teacher.find({
                            id: _.pluck(results.enrollment.course, 'teacher')
                        }).exec(cb);
                    }
                ],
                periodsForcourse: ['enrollment',
                    function(cb, results) {
                        Period.find({
                            id: _.pluck(results.enrollment.course, 'period')
                        }).exec(cb);
                    }
                ],
                courseDeep: ['teachersForcourse', 'periodsForcourse',
                    function(cb, results) {

                        var teachers = _.indexBy(results.teachersForcourse, 'id');
                        var periods = _.indexBy(results.periodsForcourse, 'id');

                        var enrollment = results.enrollment.toObject();
                        enrollment.courseMapped = _.map(enrollment.course, function(course) {
                            course.teacher = teachers[course.teacher];
                            course.period = periods[course.period];
                            return course;
                        });

                        return cb(null, enrollment);
                    }
                ]


                //Relations for student
                ,
                studentDeep: [,
                    function(cb, results) {


                        var enrollment = results.enrollment.toObject();
                        enrollment.studentMapped = _.map(enrollment.student, function(student) {
                            return student;
                        });

                        return cb(null, enrollment);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = EnrollmentPopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 23.05 minutes to type the 2305+ characters in this file.
 */