/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CourseQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      GeneratronUniversity
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var CoursePopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                course: function(cb) {
                    Course.findOne({
                        where: {
                            id: id
                        }
                    }).populate("teacher").populate("enrollments").populate("period").exec(cb);
                }


                //Relations for teacher
                ,
                teacherDeep: [,
                    function(cb, results) {


                        var course = results.course.toObject();
                        course.teacherMapped = _.map(course.teacher, function(teacher) {
                            return teacher;
                        });

                        return cb(null, course);
                    }
                ]


                //Relations for enrollments
                ,
                studentsForenrollments: ['course',
                    function(cb, results) {
                        Student.find({
                            id: _.pluck(results.course.enrollments, 'student')
                        }).exec(cb);
                    }
                ],
                enrollmentsDeep: ['studentsForenrollments',
                    function(cb, results) {

                        var students = _.indexBy(results.studentsForenrollments, 'id');

                        var course = results.course.toObject();
                        course.enrollmentsMapped = _.map(course.enrollments, function(enrollment) {
                            enrollment.student = students[enrollment.student];
                            return enrollment;
                        });

                        return cb(null, course);
                    }
                ]


                //Relations for period
                ,
                periodDeep: [,
                    function(cb, results) {


                        var course = results.course.toObject();
                        course.periodMapped = _.map(course.period, function(period) {
                            return period;
                        });

                        return cb(null, course);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = CoursePopulateDeepService;


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 21.83 minutes to type the 2183+ characters in this file.
 */