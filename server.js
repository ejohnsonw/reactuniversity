/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     server.js
Description:  Creates the app entry point\n based on code from Christophe Coenraets
Project:      GeneratronUniversity
Template: ReactSample/server.js.vmg
 */
var express = require('express'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    periods = require('./server/periods'),
    teachers = require('./server/teachers'),
    courses = require('./server/courses'),
    students = require('./server/students'),
    enrollments = require('./server/enrollments'),
    sqlinit = require('./server/sqlinit'),
    app = express();

app.set('port', process.env.PORT || 5000);

app.use(bodyParser.json());
app.use(compression());

app.use('/', express.static(__dirname + '/www'));


//Routes for Period
app.get('/periods', periods.findAll);
app.get('/periods/:id', periods.findById);
app.post('/periods', periods.createItem);
app.put('/periods', periods.updateItem);
app.delete('/periods/:id', periods.deleteItem);



//Routes for Teacher
app.get('/teachers', teachers.findAll);
app.get('/teachers/:id', teachers.findById);
app.post('/teachers', teachers.createItem);
app.put('/teachers', teachers.updateItem);
app.delete('/teachers/:id', teachers.deleteItem);



app.get('/teachers/:id/courses', courses.findByTeacher);









//Routes for Course
app.get('/courses', courses.findAll);
app.get('/courses/:id', courses.findById);
app.post('/courses', courses.createItem);
app.put('/courses', courses.updateItem);
app.delete('/courses/:id', courses.deleteItem);



app.get('/courses/:id/enrollments', enrollments.findByCourse);





//Routes for Student
app.get('/students', students.findAll);
app.get('/students/:id', students.findById);
app.post('/students', students.createItem);
app.put('/students', students.updateItem);
app.delete('/students/:id', students.deleteItem);





app.get('/students/:id/enrollments', enrollments.findByStudent);









//Routes for Enrollment
app.get('/enrollments', enrollments.findAll);
app.get('/enrollments/:id', enrollments.findById);
app.post('/enrollments', enrollments.createItem);
app.put('/enrollments', enrollments.updateItem);
app.delete('/enrollments/:id', enrollments.deleteItem);





app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send(err);
});

app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 22.62 minutes to type the 2262+ characters in this file.
 */