Generatron University 
==========================

Generatron University model is a test model we are using with [Generatron](https://www.generatron.com/).

 This repo contains 3 different applications. The source code was generated from the data model contained on model.yaml.

The backend is written using [Grails](http://grails.org/)

And I created 2 frontends, one for mobile using [Ionic Framework](http://ionicframework.com/getting-started/) and another desktop oriented using just angular [Yeoman's](http://yeoman.io/) generator-angular.


#Running
Checkout this repo and make sure you have npm and ionicframework and grails installed.

Launch the backend:


```
#!bash

cd grails
grails run-app
```



cd into the yeoman-angular directory 


```
#!bash

npm install
bower install
grunt serve --force
```


cd into the ionicframework/GeneratronUniversity

```
#!bash

npm install
bower install
ionic serve
```