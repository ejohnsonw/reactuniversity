Add the following dependencies to your Build

dependencies {
    // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
    runtime 'mysql:mysql-connector-java:5.1.29'
    test "org.grails:grails-datastore-test-support:1.0.2-grails-2.4"
	test "org.gebish:geb-spock:0.10.0"
}

plugins {
...
	//runtime "com.ngeosone.generatron:generatron-explorer:0.5.2.2"
	runtime ":cors:1.1.8"
	compile ":rest:0.8"
	test ":geb:0.10.0"
	compile ":build-test-data:2.4.0"
...
}