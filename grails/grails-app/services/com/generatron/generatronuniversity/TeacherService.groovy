/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     TeacherService.groovy
 Description:  creates a controller to process all api requests
 Project:      GeneratronUniversity
 Template: grails-24x/EntityService.groovy.vm
 */
package  com.generatron.generatronuniversity

import com.generatron.generatronuniversity.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)

class TeacherService {
	public static generatron=true
	/**
	 * 
	 * TeacherService for Teacher
	 * 
	 * 
	 */


	def addTeacher(Teacher teacher) {
		return teacher.save(flush:true)
	}

	def updateTeacher(Teacher teacher) {
		return teacher.save(flush:true)
	}

	def deleteTeacher(Long id) {
		def e = Teacher.findById(id);
		return e.delete(flush:true);
	}

	def teacher(Long id) {
		def e = Teacher.findById(id);
		if(e){
			return e;
		}else{
			return null;
		}
	}


	def teachers() {
		def results = Teacher.list();
		return results;
	}



	//From relations
	//--->courses
	def addCourseToTeacher(Long id,Course obj) {
		def e = Teacher.findById(id);
		e.addToCourses(obj);
		return e.save(flush:true);
	}

	def removeCourseFromTeacher(Long id,Course obj){
		def e = Teacher.findById(id);
		e.removeFromCourses(obj);
		return e.save(flush:true)
	}




}




/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 11.69 minutes to type the 1169+ characters in this file.
 */



