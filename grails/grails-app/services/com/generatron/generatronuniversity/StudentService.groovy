/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     StudentService.groovy
 Description:  creates a controller to process all api requests
 Project:      GeneratronUniversity
 Template: grails-24x/EntityService.groovy.vm
 */
package  com.generatron.generatronuniversity

import com.generatron.generatronuniversity.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)

class StudentService {
	public static generatron=true
	/**
	 * 
	 * StudentService for Student
	 * 
	 * 
	 */


	def addStudent(Student student) {
		return student.save(flush:true)
	}

	def updateStudent(Student student) {
		return student.save(flush:true)
	}

	def deleteStudent(Long id) {
		def e = Student.findById(id);
		return e.delete(flush:true);
	}

	def student(Long id) {
		def e = Student.findById(id);
		if(e){
			return e;
		}else{
			return null;
		}
	}


	def students() {
		def results = Student.list();
		return results;
	}



	//From relations
	//--->enrollments
	def addEnrollmentToStudent(Long id,Enrollment obj) {
		def e = Student.findById(id);
		e.addToEnrollments(obj);
		return e.save(flush:true);
	}

	def removeEnrollmentFromStudent(Long id,Enrollment obj){
		def e = Student.findById(id);
		e.removeFromEnrollments(obj);
		return e.save(flush:true)
	}




}




/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 11.97 minutes to type the 1197+ characters in this file.
 */



