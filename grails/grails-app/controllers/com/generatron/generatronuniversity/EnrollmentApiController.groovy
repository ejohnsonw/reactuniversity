/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     EnrollmentApiController.groovy
 Description:  creates a controller to process all api requests
 Project:      GeneratronUniversity
 Template: grails-24x/EntityApiController.groovy.vm
 */

package  com.generatron.generatronuniversity

import com.generatron.generatronuniversity.*;

import grails.converters.JSON;
import org.springframework.dao.DataIntegrityViolationException;

import com.generatron.annotations.*
@Generatron(explore=false, generate=false)
class EnrollmentApiController {
	public static generatron=true
	def enrollmentService;

	/**
	 * 
	 * Entity: Enrollment
	 * 
	 * 
	 */

	//---->>
	//---->>


	def create() {

		Enrollment enrollment = null;
		if(request.JSON){
			enrollment = new  Enrollment(request.JSON.model);
		}else{
			def _model = JSON.parse(params.model);
			enrollment = new Enrollment(_model);
		}


		if(!enrollment.validate()){
			response.status = 500;
			render enrollment.errors as JSON;
			return;
		}

		def result = enrollmentService.addEnrollment(enrollment)
		render result as JSON;
	}

	def update(Long id) {
		Enrollment enrollment = Enrollment.findById(id);;
		if(enrollment == null){
			response.status = 500;
			render "[{error:'Not Found'}]";
		}
		if(request.JSON){
			enrollment.properties  = request.JSON.model;
		}else{
			def _model = JSON.parse(params.model);
			enrollment = new Enrollment(_model);
		}


		if(!enrollment.validate()){
			response.status = 500;
			render enrollment.errors as JSON;

			return;
		}
		def result = enrollmentService.updateEnrollment(enrollment)
		render result as JSON;
	}


	def delete(Long id) {
		def result = enrollmentService.deleteEnrollment(id)
		//TODO: implement delete of file
		render(status:200,text:"Deleted");
	}


	def retrieve(Long id) {
		def result = enrollmentService.enrollment(id)
		if(result){
			render result as  JSON;
		}else{
			render(status:500,text:"Could not retrieve");
		}
	}


	def index() {
		def results = Enrollment.list();
		render results as JSON;
	}


	private String saveFile(Object f, String fieldName){

		//TODO: everytime the app is deployed, uploaded files are lost use something other than servletContext.getRealPath("/");
		def appDir = servletContext.getRealPath("/")
		File dirDest = new File(appDir,"/uploads/Enrollment/"+fieldName+"/")
		if(!dirDest.exists()){
			dirDest.mkdirs();
		}
		File fileDest = new File(appDir,"uploads/Enrollment/"+fieldName+"/"+f.getOriginalFilename())
		f.transferTo(fileDest)
		return fileDest.getAbsolutePath();
	}


}







/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 23.29 minutes to type the 2329+ characters in this file.
 */



