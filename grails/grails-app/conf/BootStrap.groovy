/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     BootStrap.groovy
 Description:  Authentication Filter, checks for valid access to resources
 Project:      GeneratronUniversity
 Template: grails-24x/Bootstrap.groovy.vmg
 */



import grails.converters.JSON;

import com.generatron.generatronuniversity.*;
import com.generatron.generatronuniversity.*;

import org.codehaus.groovy.grails.plugins.springsecurity.ui.*;

class BootStrap {
	def springSecurityService;
	def init = { servletContext ->

		Period.metaClass.toString = {
			->
			def toString = "";
			if(name){
				toString += name.toString();
			}

			return toString;
		};
		Teacher.metaClass.toString = {
			->
			def toString = "";
			if(firstName){
				toString += firstName.toString();
			}
			if(lastName){
				toString +=" "
				toString += lastName.toString();
			}
			if(title){
				toString +=" "
				toString += title.toString();
			}

			return toString;
		};
		Course.metaClass.toString = {
			->
			def toString = "";
			if(name){
				toString += name.toString();
			}

			return toString;
		};
		Student.metaClass.toString = {
			->
			def toString = "";
			if(firstName){
				toString += firstName.toString();
			}
			if(lastName){
				toString +=" "
				toString += lastName.toString();
			}

			return toString;
		};
		Enrollment.metaClass.toString = {
			->
			def toString = "";

			return toString;
		};

		JSON.registerObjectMarshaller(Period) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.name){
				returnArray['name'] = it.name
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Teacher) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.address){
				returnArray['address'] = it.address
			}
			if(it.city){
				returnArray['city'] = it.city
			}
			if(it.courses){
				returnArray['courses'] = it.courses
			}
			if(it.email){
				returnArray['email'] = it.email
			}
			if(it.firstName){
				returnArray['firstName'] = it.firstName
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.lastName){
				returnArray['lastName'] = it.lastName
			}
			if(it.mobilePhone){
				returnArray['mobilePhone'] = it.mobilePhone
			}
			if(it.phone){
				returnArray['phone'] = it.phone
			}
			if(it.pic){
				returnArray['pic'] = it.pic
			}
			if(it.state){
				returnArray['state'] = it.state
			}
			if(it.title){
				returnArray['title'] = it.title
			}
			if(it.zip){
				returnArray['zip'] = it.zip
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Course) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.code){
				returnArray['code'] = it.code
			}
			if(it.credits){
				returnArray['credits'] = it.credits
			}
			if(it.enrollments){
				returnArray['enrollments'] = it.enrollments
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.name){
				returnArray['name'] = it.name
			}
			if(it.period){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.period.id;
				//domainArray['toString']=it.period.toString();
				//returnArray['period'] = domainArray;

				//this returns the full serialized object
				returnArray['period'] = it.period;

			}
			if(it.teacher){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.teacher.id;
				//domainArray['toString']=it.teacher.toString();
				//returnArray['teacher'] = domainArray;

				//this returns the full serialized object
				returnArray['teacher'] = it.teacher;

			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Student) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.address){
				returnArray['address'] = it.address
			}
			if(it.city){
				returnArray['city'] = it.city
			}
			if(it.dob){
				returnArray['dob'] = it.dob
			}
			if(it.email){
				returnArray['email'] = it.email
			}
			if(it.enrollments){
				returnArray['enrollments'] = it.enrollments
			}
			if(it.firstName){
				returnArray['firstName'] = it.firstName
			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.lastName){
				returnArray['lastName'] = it.lastName
			}
			if(it.lastupdate){
				returnArray['lastupdate'] = it.lastupdate
			}
			if(it.mobilePhone){
				returnArray['mobilePhone'] = it.mobilePhone
			}
			if(it.phone){
				returnArray['phone'] = it.phone
			}
			if(it.pic){
				returnArray['pic'] = it.pic
			}
			if(it.registration){
				returnArray['registration'] = it.registration
			}
			if(it.state){
				returnArray['state'] = it.state
			}
			if(it.zip){
				returnArray['zip'] = it.zip
			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
		JSON.registerObjectMarshaller(Enrollment) {
			def returnArray = [:]
			//returnArray['class_'] = it.class
			if(it.course){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.course.id;
				//domainArray['toString']=it.course.toString();
				//returnArray['course'] = domainArray;

				//this returns the full serialized object
				returnArray['course'] = it.course;

			}
			if(it.id){
				returnArray['id'] = it.id
			}
			if(it.student){
				//uncomment this to return whatever is being constructed on the toString
				//def domainArray = [:]
				//domainArray['id']=it.student.id;
				//domainArray['toString']=it.student.toString();
				//returnArray['student'] = domainArray;

				//this returns the full serialized object
				returnArray['student'] = it.student;

			}

			returnArray['toString'] = it.toString();
			return returnArray
		}
	}
	def destroy = {
	}
}


/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 58.47 minutes to type the 5847+ characters in this file.
 */



