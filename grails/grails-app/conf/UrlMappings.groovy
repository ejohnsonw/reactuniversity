/* 
 Copyright (c) 2015 NgeosOne LLC
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 Engineered using http://www.generatron.com/
 [GENERATRON]
 Generator :   System Templates
 Filename:     UrlMappings.groovy
 Description:  URL Mappings
 Project:      GeneratronUniversity
 Template: grails-24x/URLMappings.groovy.vmg
 */

class UrlMappings {
	static mappings = {
		"/$controller/$action?/$id?(.$format)?"{ constraints { // apply constraints here
			} }

		"/"(view:"/index")
		"500"(view:'/error')

		//routes for Period
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/period/$id"(controller:"PeriodApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/period"(controller:"PeriodApi"){
			action = [GET:"index", POST:"create"]
		}


		//routes for Teacher
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/teacher/$id"(controller:"TeacherApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/teacher"(controller:"TeacherApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/teacher/$id/addCourseToTeacher"(controller:"TeacherApi",action:"addCourseToTeacher")
		"/api/teacher/$id/removeCourseFromTeacher"(controller:"TeacherApi",action:"removeCourseFromTeacher")

		//routes for Course
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/course/$id"(controller:"CourseApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/course"(controller:"CourseApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/course/$id/addEnrollmentToCourse"(controller:"CourseApi",action:"addEnrollmentToCourse")
		"/api/course/$id/removeEnrollmentFromCourse"(controller:"CourseApi",action:"removeEnrollmentFromCourse")

		//routes for Student
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/student/$id"(controller:"StudentApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/student"(controller:"StudentApi"){
			action = [GET:"index", POST:"create"]
		}

		"/api/student/$id/addEnrollmentToStudent"(controller:"StudentApi",action:"addEnrollmentToStudent")
		"/api/student/$id/removeEnrollmentFromStudent"(controller:"StudentApi",action:"removeEnrollmentFromStudent")

		//routes for Enrollment
		//using POST:update instead of put, because PUT multiform with files is not interpreted correctly by spring framework
		"/api/enrollment/$id"(controller:"EnrollmentApi"){
			action = [GET:"retrieve", POST:"update", DELETE:"delete"]
		}
		"/api/enrollment"(controller:"EnrollmentApi"){
			action = [GET:"index", POST:"create"]
		}



	}
}

/* 
 [TRIVIA]
 It would take a person typing  @ 100.0 cpm, 
 approximately 25.88 minutes to type the 2588+ characters in this file.
 */



