/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     PeriodEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:PeriodsCtrl
 * @description
 * # PeriodsCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('PeriodEditCtrl', function($scope, $routeParams, $rootScope, PeriodWS) {
        $scope.period = {};
        $scope.edit = true;
        $scope.save = function() {
            PeriodWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.period);
        }

        $scope.delete = function() {
            PeriodWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.period);
        }
        $scope.cancel = function() {
            document.location.href = "#/periods"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/periods"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.period = data;
            // false String String id
            // false String String name
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // false String String id
        // false String String name

        $scope.loadReferences = function() {

            // false String String id
            // false String String name

        }
        $scope.loadReferences();
        PeriodWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/period/:id', {
                templateUrl: 'views/periodForm.html',
                controller: 'PeriodEditCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 17.01 minutes to type the 1701+ characters in this file.
 */