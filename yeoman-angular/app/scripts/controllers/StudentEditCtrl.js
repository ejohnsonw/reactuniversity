/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     StudentEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:StudentsCtrl
 * @description
 * # StudentsCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('StudentEditCtrl', function($scope, $routeParams, $rootScope, StudentWS, EnrollmentWS) {
        $scope.student = {};
        $scope.edit = true;
        $scope.save = function() {
            StudentWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.student);
        }

        $scope.delete = function() {
            StudentWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.student);
        }
        $scope.cancel = function() {
            document.location.href = "#/students"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/students"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.student = data;
            // false String String address
            // false String String city
            // false Date String dob
            // false String String email
            // true Collection Enrollment enrollments
            if (typeof $scope.student.enrollments == "undefined") {
                $scope.student.enrollments = [];
            }

            // false String String firstName
            // false String String id
            // false String String lastName
            // false timestamp String lastupdate
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false timestamp String registration
            // false String String state
            // false String String zip
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // false String String address
        // false String String city
        // false Date String dob
        // false String String email
        // true Collection Enrollment enrollments
        if (typeof $scope.student.enrollments == "undefined") {
            $scope.student.enrollments = [];
        }

        $scope.selectedEnrollment = function(enrollments, isCheck) {
                var idx = findIndexId($scope.student.enrollments, enrollments.id)

                if (idx == -1) {
                    if (typeof isCheck == "undefined") {
                        $scope.student.enrollments.push(enrollments);
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    if (typeof isCheck == "undefined") {
                        $scope.student.enrollments.splice(idx, 1);
                        return true;
                    } else {
                        return true;
                    }
                }

            }
            // false String String firstName
            // false String String id
            // false String String lastName
            // false timestamp String lastupdate
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false timestamp String registration
            // false String String state
            // false String String zip

        $scope.loadReferences = function() {

            // false String String address
            // false String String city
            // false Date String dob
            // false String String email
            // true Collection Enrollment enrollments
            EnrollmentWS.list(function(data) {
                    $scope.enrollmentss = data;
                }, $scope.errorcb)
                // false String String firstName
                // false String String id
                // false String String lastName
                // false timestamp String lastupdate
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false timestamp String registration
                // false String String state
                // false String String zip

        }
        $scope.loadReferences();
        StudentWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/student/:id', {
                templateUrl: 'views/studentForm.html',
                controller: 'StudentEditCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 47.09 minutes to type the 4709+ characters in this file.
 */