/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     RegisterCtrl.js
Description:  Creates a controller to register into the system
Project:      GeneratronUniversity
Template: yeoman-angular/register.controller.js.vmg
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:EnrollmentsCtrl
 * @description
 * # EnrollmentsCtrl
 *
 */
angular.module('GeneratronUniversity')
    .controller('RegisterCtrl', function($scope, webservice, $routeParams, $rootScope) {
        $scope.user = {}

        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/"
        }

        $scope.errorcb = function(data, status, headers, config) {
            alert("Failed: " + JSON.stringify(data));
        }

        $scope.register = function() {
            webservice.register($scope.successcb, $scope.errorcb, $scope.user);
        }
    }).config(function($routeProvider) {
        $routeProvider
            .when('/register', {
                templateUrl: 'register.html',
                controller: 'RegisterCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 8.62 minutes to type the 862+ characters in this file.
 */