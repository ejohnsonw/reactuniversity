/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UploadService.js
Description:  Angular service to access webservices endpoints
Project:      GeneratronUniversity
Template: /yeoman-angular/services/uploaderservice.vm
 */
'use strict';


(function() {
    'use strict';

    angular.module('GeneratronUniversity.services', [])
        .factory('UploadService', [
            '$q',
            function($q) {
                return {

                    request: function(method, url, fieldId, config) {
                        var files = document.getElementById(fieldId).files
                        var deferred = $q.defer();

                        //
                        // If there are no files then `$http.post` should
                        // be used instead.
                        //
                        if (files == null) {
                            deferred.reject('There\'s no files to upload');
                            return deferred.promise;
                        }

                        var xhr = new XMLHttpRequest();

                        xhr.upload.onprogress = function(event) {
                            if (event.lengthComputable) {
                                deferred.notify(Math.round((event.loaded / event.total) * 100));
                            }
                        };

                        xhr.onload = function() {
                            var use = 'reject';
                            if (xhr.status >= 200 && xhr.status < 300)
                                use = 'resolve';

                            deferred[use]({
                                files: files,
                                dataText: xhr.responseText,
                                data: xhr.response,
                                status: xhr.status,
                                statusText: xhr.statusText
                            });
                        };

                        xhr.upload.onerror = function() {
                            var msg = xhr.responseText ? xhr.responseText : 'Error occurred uploading to ' + url;

                            deferred.reject(msg);
                            return deferred.promise;
                        };

                        var fd = new FormData();

                        //
                        // There's data to be added to the payload on this upload?
                        //
                        if (config && config.data != null) {
                            // If it's an Object then run over it, if not add it as `data` field.
                            /*if (typeof config.data === 'object') {
                              Object
                                .keys(config.data)
                                .forEach(function (element) {
                                  fd.append(element, config.data[element]);
                                }
                              );
                            } else {
                              fd.append('data', config.data);
                            }*/
                            fd.append('model', JSON.stringify(config.data));
                        }

                        function getNewFilename(fileName, baseName) {
                            //
                            // The match for extension is made for characters from 3 to 4 from the
                            // final point. An extension with more than 4 characters and less than
                            // 3 will be null. This includes the point so it would end like:
                            //
                            //     filename = "string.png";
                            //     extension = ".png";
                            //
                            var extension = fileName.match(/\..{3,4}$/)[0];
                            var newFilename = baseName + extension;
                            return newFilename;
                        }

                        //
                        // Load the files in the formData object
                        //
                        debugger;
                        if (files.length) {
                            for (var i = 0; i < files.length; i++) {
                                if (config && config.newNames && config.newNames[files[i].name] != null) {
                                    var newName = getNewFilename(files[i].name, config.newNames[files[i].name]);

                                    fd.append("_" + fieldId, files[i], newName);
                                } else {
                                    fd.append("_" + fieldId, files[i]);
                                }
                            }
                        } else if (files && config && config.newNames) {
                            /* jshint -W004 */
                            var newName = getNewFilename(files.name, config.newNames[files.name]);
                            /* jshing +W004 */

                            fd.append("_" + fieldId, files, newName);
                        } else {
                            fd.append("_" + fieldId, files);
                        }

                        //
                        // Send the files
                        //
                        xhr.open(method, url);

                        //
                        // Adding configs
                        // --------------
                        // 
                        //             //
                        if (config && config.headers != null) {
                            Object.keys(config.headers).forEach(function(element) {
                                xhr.setRequestHeader(element, config.headers[element]);
                            });
                        }

                        //
                        //             //
                        if (config && config.withCredentials != null) {
                            xhr.withCredentials = config.withCredentials;
                        }

                        xhr.send(fd);

                        return deferred.promise;

                    },
                    post: function(url, fieldId, config) {
                        return this.request('POST', url, fieldId, config);
                    },
                    send: function(url, fieldId, config) {
                        return this.request('POST', url, fieldId, config);
                    },
                    put: function(url, fieldId, config) {
                        return this.request('PUT', url, fieldId, config);
                    }
                };
            }
        ]);
})();


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 48.9 minutes to type the 4890+ characters in this file.
 */