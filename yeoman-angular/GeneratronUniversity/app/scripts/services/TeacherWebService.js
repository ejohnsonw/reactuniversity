/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     TeacherWebService.js
Description:  Angular service to access webservices endpoints
Project:      GeneratronUniversity
Template: /yeoman-angular/services/webservice.vm
 */


'use strict';

/**
 * @ngdoc service
 * @name GeneratronUniversityApp.webservice
 * @description
 * # webservice
 * Service in the GeneratronUniversityApp.
 */

//Set the url to the backend service here
angular.module('GeneratronUniversity.services')
    .service('TeacherWS', function($http, UploadService) {

        var teachers = [];
        var teacher = {};
        return {
            /*
             * Teacher Methods
             *
             */
            getTeachers: function() {
                return teachers;
            },
            setTeachers: function(data) {
                teachers = data;
            },
            //Should be a blueprint endpoint
            retrieve: function(cb, cberror, id) {
                var url = config.baseurl + "teacher/" + id;
                console.log(url);
                $http.get(url).success(cb).error(cberror);

            },
            list: function(cb, cberror) {
                var url = config.baseurl + "teacher";
                console.log(url);
                $http.get(url).success(cb).error(cberror);
            }

            ,
            create: function(cb, cberror, params) {
                var url = config.baseurl + "teacher";
                //UploadService.post(url,'${attribute.name}',{data:params}).then(cb, cberror, null);
                var model = {
                    model: params
                };
                $http.post(url, model).success(cb).error(cberror);


            },
            update: function(cb, cberror, params) {
                var url = config.baseurl + "teacher/" + params.id;
                //UploadService.post(url,'${attribute.name}',{data:params}).then(cb, cberror, null);
                var model = {
                    model: params
                };
                $http.post(url, model).success(cb).error(cberror);
            },
            delete: function(cb, cberror, params) {
                var url = config.baseurl + "teacher/" + params.id;
                var model = {
                    model: params
                };
                $http.delete(url, model).success(cb).error(cberror);
            }

            //Now on to the declared routes

        } // closes return 
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 21.23 minutes to type the 2123+ characters in this file.
 */