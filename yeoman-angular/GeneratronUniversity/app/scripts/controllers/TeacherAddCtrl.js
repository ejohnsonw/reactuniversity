/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     TeacherAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:TeacherAddCtrl
 * @description
 * # TeacherAddCtrl
 * Controller of the GeneratronUniversity
 */
angular.module('GeneratronUniversity.controllers')
    .controller('TeacherAddCtrl', function($scope, CourseWS, TeacherWS) {
        $scope.teacher = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/teachers"
                // false String String address
                // false String String city
                // true Collection Course courses
            if (typeof $scope.teacher.courses == "undefined") {
                $scope.teacher.courses = [];
            }
            // false String String email
            // false String String firstName
            // false String String id
            // false String String lastName
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false String String state
            // false String String title
            // false String String zip
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            TeacherWS.create($scope.successcb, $scope.errorcb, $scope.teacher);
        }

        $scope.cancel = function() {
            document.location.href = "#/teachers"
        }

        // false String String address
        // false String String city
        // true Collection Course courses
        $scope.teacher.courses = [];
        $scope.selectedCourse = function(courses) {
            var idx = $scope.teacher.courses.indexOf(courses)
            if (idx == -1) {
                $scope.teacher.courses.push(courses);
            } else {
                $scope.teacher.courses.splice(idx, 1);
            }
            console.log($scope.teacher.courses)
        }

        // false String String email
        // false String String firstName
        // false String String id
        // false String String lastName
        // false String String mobilePhone
        // false String String phone
        // false String String pic
        // false String String state
        // false String String title
        // false String String zip

        $scope.loadReferences = function() {
            // false String String address
            // false String String city
            // true Collection Course courses
            CourseWS.list(function(data) {
                    $scope.coursess = data;
                }, $scope.errorcb)
                // false String String email
                // false String String firstName
                // false String String id
                // false String String lastName
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false String String state
                // false String String title
                // false String String zip

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addTeacher', {
                templateUrl: 'views/teacherForm.html',
                controller: 'TeacherAddCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 38.09 minutes to type the 3809+ characters in this file.
 */