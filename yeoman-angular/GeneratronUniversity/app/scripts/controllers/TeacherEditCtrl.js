/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     TeacherEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:TeachersCtrl
 * @description
 * # TeachersCtrl
 *
 */
angular.module('GeneratronUniversity.controllers')
    .controller('TeacherEditCtrl', function($scope, $routeParams, $rootScope, CourseWS, TeacherWS) {
        $scope.teacher = {};
        $scope.edit = true;
        $scope.save = function() {
            TeacherWS.update($scope.updatesuccesscb, $scope.errorcb, $scope.teacher);
        }

        $scope.delete = function() {
            TeacherWS.delete($scope.updatesuccesscb, $scope.errorcb, $scope.teacher);
        }
        $scope.cancel = function() {
            document.location.href = "#/teachers"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/teachers"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.teacher = data;
            // false String String address
            // false String String city
            // true Collection Course courses
            if (typeof $scope.teacher.courses == "undefined") {
                $scope.teacher.courses = [];
            }

            // false String String email
            // false String String firstName
            // false String String id
            // false String String lastName
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false String String state
            // false String String title
            // false String String zip
        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }

        // false String String address
        // false String String city
        // true Collection Course courses
        if (typeof $scope.teacher.courses == "undefined") {
            $scope.teacher.courses = [];
        }

        $scope.selectedCourse = function(courses, isCheck) {
                var idx = findIndexId($scope.teacher.courses, courses.id)

                if (idx == -1) {
                    if (typeof isCheck == "undefined") {
                        $scope.teacher.courses.push(courses);
                        return false;
                    } else {
                        return false;
                    }
                } else {
                    if (typeof isCheck == "undefined") {
                        $scope.teacher.courses.splice(idx, 1);
                        return true;
                    } else {
                        return true;
                    }
                }

            }
            // false String String email
            // false String String firstName
            // false String String id
            // false String String lastName
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false String String state
            // false String String title
            // false String String zip

        $scope.loadReferences = function() {

            // false String String address
            // false String String city
            // true Collection Course courses
            CourseWS.list(function(data) {
                    $scope.coursess = data;
                }, $scope.errorcb)
                // false String String email
                // false String String firstName
                // false String String id
                // false String String lastName
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false String String state
                // false String String title
                // false String String zip

        }
        $scope.loadReferences();
        TeacherWS.retrieve($scope.successcb, $scope.errorcb, $routeParams.id);
    }).config(function($routeProvider) {
        $routeProvider
            .when('/teacher/:id', {
                templateUrl: 'views/teacherForm.html',
                controller: 'TeacherEditCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 42.95 minutes to type the 4295+ characters in this file.
 */