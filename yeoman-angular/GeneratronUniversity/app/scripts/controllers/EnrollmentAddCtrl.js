/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     EnrollmentAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:EnrollmentAddCtrl
 * @description
 * # EnrollmentAddCtrl
 * Controller of the GeneratronUniversity
 */
angular.module('GeneratronUniversity.controllers')
    .controller('EnrollmentAddCtrl', function($scope, StudentWS, CourseWS, EnrollmentWS) {
        $scope.enrollment = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/enrollments"
                // false Course String course
                // false String String id
                // false Student String student
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            EnrollmentWS.create($scope.successcb, $scope.errorcb, $scope.enrollment);
        }

        $scope.cancel = function() {
            document.location.href = "#/enrollments"
        }

        // false Course String course
        // false String String id
        // false Student String student

        $scope.loadReferences = function() {
            // false Course String course
            CourseWS.list(function(data) {
                    $scope.courses = data;
                }, $scope.errorcb)
                // false String String id
                // false Student String student
            StudentWS.list(function(data) {
                $scope.students = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addEnrollment', {
                templateUrl: 'views/enrollmentForm.html',
                controller: 'EnrollmentAddCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 20.0 minutes to type the 2000+ characters in this file.
 */