/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     StudentAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      GeneratronUniversity
Template: /yeoman-angular/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name GeneratronUniversity.controller:StudentAddCtrl
 * @description
 * # StudentAddCtrl
 * Controller of the GeneratronUniversity
 */
angular.module('GeneratronUniversity.controllers')
    .controller('StudentAddCtrl', function($scope, StudentWS, EnrollmentWS) {
        $scope.student = {};
        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/students"
                // false String String address
                // false String String city
                // false Date String dob
                // false String String email
                // true Collection Enrollment enrollments
            if (typeof $scope.student.enrollments == "undefined") {
                $scope.student.enrollments = [];
            }
            // false String String firstName
            // false String String id
            // false String String lastName
            // false timestamp String lastupdate
            // false String String mobilePhone
            // false String String phone
            // false String String pic
            // false timestamp String registration
            // false String String state
            // false String String zip
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            StudentWS.create($scope.successcb, $scope.errorcb, $scope.student);
        }

        $scope.cancel = function() {
            document.location.href = "#/students"
        }

        // false String String address
        // false String String city
        // false Date String dob
        // false String String email
        // true Collection Enrollment enrollments
        $scope.student.enrollments = [];
        $scope.selectedEnrollment = function(enrollments) {
            var idx = $scope.student.enrollments.indexOf(enrollments)
            if (idx == -1) {
                $scope.student.enrollments.push(enrollments);
            } else {
                $scope.student.enrollments.splice(idx, 1);
            }
            console.log($scope.student.enrollments)
        }

        // false String String firstName
        // false String String id
        // false String String lastName
        // false timestamp String lastupdate
        // false String String mobilePhone
        // false String String phone
        // false String String pic
        // false timestamp String registration
        // false String String state
        // false String String zip

        $scope.loadReferences = function() {
            // false String String address
            // false String String city
            // false Date String dob
            // false String String email
            // true Collection Enrollment enrollments
            EnrollmentWS.list(function(data) {
                    $scope.enrollmentss = data;
                }, $scope.errorcb)
                // false String String firstName
                // false String String id
                // false String String lastName
                // false timestamp String lastupdate
                // false String String mobilePhone
                // false String String phone
                // false String String pic
                // false timestamp String registration
                // false String String state
                // false String String zip

        }
        $scope.loadReferences();


    }).config(function($routeProvider) {
        $routeProvider
            .when('/addStudent', {
                templateUrl: 'views/studentForm.html',
                controller: 'StudentAddCtrl'
            })
    });


/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 42.23 minutes to type the 4223+ characters in this file.
 */