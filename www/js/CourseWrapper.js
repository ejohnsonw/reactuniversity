/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     CourseWrapper.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityWrapper.js.vm
 */
import React from 'react/addons';

import CourseForm from './CourseForm';

export default React.createClass({

    saveHandler() {
            this.refs.form.save();
        },

        savedHandler() {
            window.location.hash = "#course/" + this.props.course.id;
        },

        render() {
            return ( < div className = "slds-m-around--medium" >
                < CourseForm ref = "form"
                course = {
                    this.props.course
                }
                onSaved = {
                    this.savedHandler
                }
                /> < button className = "slds-button slds-button--neutral slds-button--brand slds-m-around--small"
                onClick = {
                    this.saveHandler
                } > Save < /button> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 6.35 minutes to type the 635+ characters in this file.
 */