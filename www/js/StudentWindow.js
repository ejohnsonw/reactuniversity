/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     StudentWindow.js
Description:  The Window
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityWindow.js.vm
 */
import React from 'react';
import StudentForm from './StudentForm';

export default React.createClass({

    saveHandler() {
            this.refs.form.save();
        },

        render() {
            return ( < div >
                < div aria - hidden = "false"
                role = "dialog"
                className = "slds-modal slds-fade-in-open" >
                < div className = "slds-modal__container" >
                < div className = "slds-modal__header" >
                < h2 className = "slds-text-heading--medium" > New Student < /h2> < button className = "slds-button slds-modal__close" >
                < svg aria - hidden = "true"
                className = "slds-button__icon slds-button__icon--inverse slds-button__icon--large" >
                < /svg> < span className = "slds-assistive-text" > Close < /span> < /button> < /div> < div className = "slds-modal__content"
                style = {
                    {
                        overflow: "visible"
                    }
                } >
                < StudentForm ref = "form"
                onSaved = {
                    this.props.onSaved
                }
                /> < /div>

                < div className = "slds-modal__footer" >
                < button className = "slds-button slds-button--neutral"
                onClick = {
                    this.props.onCancel
                } > Cancel < /button> < button className = "slds-button slds-button--neutral slds-button--brand"
                onClick = {
                    this.saveHandler
                } > Save < /button> < /div> < /div> < /div> < div className = "slds-modal-backdrop slds-modal-backdrop--open" > < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 16.7 minutes to type the 1670+ characters in this file.
 */