/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     TeacherHome.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityHome.js.vm
 */
import React from 'react';

import * as TeacherService from './services/TeacherService';

import settings from './services/settings';

import {
    HomeHeader
}
from './components/PageHeader';

import TeacherList from './TeacherList';
import TeacherFormWindow from './TeacherFormWindow';


export default React.createClass({

            getInitialState() {
                    return {
                        teachers: [],
                    };
                },

                componentDidMount() {
                    PeriodService.findAll().then(periods => this.setState({
                        periods
                    }));

                },

                editHandler(data) {
                    window.location.hash = "#teacher/" + data.id + "/edit";
                },

                viewChangeHandler(index, periodId, label) {
                    TeacherService.findAll({
                        periodId
                    }).then(teachers => this.setState({
                        periodId, teachers
                    }));
                },

                newHandler() {
                    this.setState({
                        addingTeacher: true
                    });
                },

                savedHandler(student) {
                    this.setState({
                        addingTeacher: false
                    });
                    window.location.hash = "#teacher/" + teacher.id;
                },

                cancelHandler() {
                    this.setState({
                        addingTeacher: false
                    });
                },

                render() {
                    return ( < div >
                        < HomeHeader type = "Teachers"
                        title = "Current Teachers"
                        newLabel = "New Teacher"
                        actions = {
                            [{
                                value: "new",
                                label: "New Teacher"
                            }]
                        }
                        itemCount = {
                            this.state.teachers.length
                        }
                        onViewChange = {
                            this.viewChangeHandler
                        }
                        onNew = {
                            this.newHandler
                        }
                        /> < TeacherList teachers = {
                            this.state.teachers
                        }
                        /> {
                            this.state.addingTeacher ? < TeacherFormWindow onSaved = {
                                this.savedHandler
                            }
                            onCancel = {
                                this.cancelHandler
                            }
                            />:null} < /div>
                        );
                    }

                });

        /* 
        [TRIVIA]
        It would take a person typing  @ 100.0 cpm, 
        approximately 17.3 minutes to type the 1730+ characters in this file.
         */