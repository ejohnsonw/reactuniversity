/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     TeacherRecord.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityRecord.js.vm
 */
import React from 'react';
import moment from 'moment';

import * as TeacherService from './services/TeacherService';

import {
    RecordHeader, HeaderField
}
from './components/PageHeader';

import TeacherView from './TeacherView';
import TeacherEnrollmentCard from './TeacherEnrollmentCard';

export default React.createClass({

    getInitialState() {
            return {
                teacher: {}
            };
        },

        componentDidMount() {
            this.getTeacher(this.props.params.teacherId);
        },

        componentWillReceiveProps(props) {
            this.getTeacher(props.params.teacherId);
        },

        getTeacher(id) {
            TeacherService.findById(id).then(teacher => this.setState({
                teacher
            }));
        },

        deleteHandler() {
            TeacherService.deleteItem(this.state.teacher.id).then(() => window.location.hash = "teachers");
        },

        editHandler() {
            window.location.hash = "#teacher/" + this.state.teacher.id + "/edit";
        },

        render() {
            return ( < div >
                < RecordHeader type = "Teacher"
                icon = "orders"
                title = {
                    this.state.teacher.name
                }
                onEdit = {
                    this.editHandler
                }
                onDelete = {
                    this.deleteHandler
                } >
                < HeaderField label = "FirstName"
                value = {
                    this.state.teacher.firstName
                }
                /> < HeaderField label = "LastName"
                value = {
                    this.state.teacher.lastName
                }
                /> < HeaderField label = "Title"
                value = {
                    this.state.teacher.title
                }
                /> < /RecordHeader>

                {
                    React.cloneElement(this.props.children, {
                        teacher: this.state.teacher
                    })
                }

                < /div>
            );
        }
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 16.08 minutes to type the 1608+ characters in this file.
 */