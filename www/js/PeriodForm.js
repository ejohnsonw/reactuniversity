/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     PeriodForm.js
Description:  Captures information for an entity
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityForm.js.vm
 */
import React from 'react/addons';

import * as PeriodService from './services/PeriodService';

import ComboBox from './components/ComboBox';

export default React.createClass({

    getInitialState() {
            return {
                period: {},
            };
        },

        componentWillReceiveProps(props) {
            let period = props.period;
            this.setState({
                period
            });
        },

        componentDidMount() {},

        idChangeHandler(event) {
            let period = this.state.period;
            period.id = event.target.value;
            this.setState({
                period
            });
        }
    nameChangeHandler(event) {
        let period = this.state.period;
        period.name = event.target.value;
        this.setState({
            period
        });
    }

    save() {
            let saveItem = this.state.period.id ? PeriodService.updateItem : PeriodService.createItem;
            saveItem(this.state.period).then(savedPeriod => {
                if (this.props.onSaved) this.props.onSaved(savedPeriod);
            });
        },

        render() {
            let period = this.state.period;
            return ( < div className = "slds-form--stacked slds-grid slds-wrap" >
                < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2" >
                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Id < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    period.id
                }
                onChange = {
                    this.idChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Name < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    period.name
                }
                onChange = {
                    this.nameChangeHandler
                }
                /> < /div> < /div> < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 22.08 minutes to type the 2208+ characters in this file.
 */