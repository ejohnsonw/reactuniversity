/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     app.js
Description:  app
Project:      GeneratronUniversity
Template: /ReactSample/www/js/app.js.vmg
 */
import React from 'react';
import {
    Router, Route, IndexRoute
}
from 'react-router';

import PeriodHome from './PeriodHome';
import PeriodRecord from './PeriodRecord';
import PeriodView from './PeriodView';
import PeriodFormWrapper from './PeriodFormWrapper';
import TeacherHome from './TeacherHome';
import TeacherRecord from './TeacherRecord';
import TeacherView from './TeacherView';
import TeacherFormWrapper from './TeacherFormWrapper';
import CourseHome from './CourseHome';
import CourseRecord from './CourseRecord';
import CourseView from './CourseView';
import CourseFormWrapper from './CourseFormWrapper';
import StudentHome from './StudentHome';
import StudentRecord from './StudentRecord';
import StudentView from './StudentView';
import StudentFormWrapper from './StudentFormWrapper';
import EnrollmentHome from './EnrollmentHome';
import EnrollmentRecord from './EnrollmentRecord';
import EnrollmentView from './EnrollmentView';
import EnrollmentFormWrapper from './EnrollmentFormWrapper';

React.render(( < Router >
    < Route path = "/"
    component = {
        Shell
    } >
    < IndexRoute component = {
        StudentHome
    }
    /> < Route path = "periods"
    component = {
        PeriodHome
    }
    /> < Route path = "period"
    component = {
        PeriodRecord
    } >
    < Route path = ":periodId"
    component = {
        PeriodView
    }
    /> < Route path = ":periodId/edit"
    component = {
        PeriodFormWrapper
    }
    /> < /Route> < Route path = "teachers"
    component = {
        TeacherHome
    }
    /> < Route path = "teacher"
    component = {
        TeacherRecord
    } >
    < Route path = ":teacherId"
    component = {
        TeacherView
    }
    /> < Route path = ":teacherId/edit"
    component = {
        TeacherFormWrapper
    }
    /> < /Route> < Route path = "courses"
    component = {
        CourseHome
    }
    /> < Route path = "course"
    component = {
        CourseRecord
    } >
    < Route path = ":courseId"
    component = {
        CourseView
    }
    /> < Route path = ":courseId/edit"
    component = {
        CourseFormWrapper
    }
    /> < /Route> < Route path = "students"
    component = {
        StudentHome
    }
    /> < Route path = "student"
    component = {
        StudentRecord
    } >
    < Route path = ":studentId"
    component = {
        StudentView
    }
    /> < Route path = ":studentId/edit"
    component = {
        StudentFormWrapper
    }
    /> < /Route> < Route path = "enrollments"
    component = {
        EnrollmentHome
    }
    /> < Route path = "enrollment"
    component = {
        EnrollmentRecord
    } >
    < Route path = ":enrollmentId"
    component = {
        EnrollmentView
    }
    /> < Route path = ":enrollmentId/edit"
    component = {
        EnrollmentFormWrapper
    }
    /> < /Route> < Route path = "*"
    component = {
        StudentHome
    }
    /> < /Route> < /Router>
), document.body);

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 27.44 minutes to type the 2744+ characters in this file.
 */