/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     TeacherForm.js
Description:  Captures information for an entity
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityForm.js.vm
 */
import React from 'react/addons';

import * as TeacherService from './services/TeacherService';

import ComboBox from './components/ComboBox';

export default React.createClass({

    getInitialState() {
            return {
                teacher: {},
            };
        },

        componentWillReceiveProps(props) {
            let teacher = props.teacher;
            this.setState({
                teacher
            });
        },

        componentDidMount() {},

        addressChangeHandler(event) {
            let teacher = this.state.teacher;
            teacher.address = event.target.value;
            this.setState({
                teacher
            });
        }
    cityChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.city = event.target.value;
        this.setState({
            teacher
        });
    }, coursesChangeHandler(index, value, label) {
        // Should add to collection
        /*let teacher = this.state.teacher;
        teacher.courses.id = value;
        this.setState({teacher});*/
    }, emailChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.email = event.target.value;
        this.setState({
            teacher
        });
    }, firstNameChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.firstName = event.target.value;
        this.setState({
            teacher
        });
    }, idChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.id = event.target.value;
        this.setState({
            teacher
        });
    }, lastNameChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.lastName = event.target.value;
        this.setState({
            teacher
        });
    }, mobilePhoneChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.mobilePhone = event.target.value;
        this.setState({
            teacher
        });
    }, phoneChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.phone = event.target.value;
        this.setState({
            teacher
        });
    }, picChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.pic = event.target.value;
        this.setState({
            teacher
        });
    }, stateChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.state = event.target.value;
        this.setState({
            teacher
        });
    }, titleChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.title = event.target.value;
        this.setState({
            teacher
        });
    }, zipChangeHandler(event) {
        let teacher = this.state.teacher;
        teacher.zip = event.target.value;
        this.setState({
            teacher
        });
    }

    save() {
            let saveItem = this.state.teacher.id ? TeacherService.updateItem : TeacherService.createItem;
            saveItem(this.state.teacher).then(savedTeacher => {
                if (this.props.onSaved) this.props.onSaved(savedTeacher);
            });
        },

        render() {
            let teacher = this.state.teacher;
            return ( < div className = "slds-form--stacked slds-grid slds-wrap" >
                < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2" >
                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Address < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.address
                }
                onChange = {
                    this.addressChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > City < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.city
                }
                onChange = {
                    this.cityChangeHandler
                }
                /> < /div> < /div>

                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Email < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.email
                }
                onChange = {
                    this.emailChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > FirstName < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.firstName
                }
                onChange = {
                    this.firstNameChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Id < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.id
                }
                onChange = {
                    this.idChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > LastName < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.lastName
                }
                onChange = {
                    this.lastNameChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > MobilePhone < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.mobilePhone
                }
                onChange = {
                    this.mobilePhoneChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Phone < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.phone
                }
                onChange = {
                    this.phoneChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Pic < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.pic
                }
                onChange = {
                    this.picChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > State < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.state
                }
                onChange = {
                    this.stateChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Title < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.title
                }
                onChange = {
                    this.titleChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Zip < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    teacher.zip
                }
                onChange = {
                    this.zipChangeHandler
                }
                /> < /div> < /div> < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 89.86 minutes to type the 8986+ characters in this file.
 */