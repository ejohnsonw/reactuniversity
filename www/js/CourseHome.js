/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     CourseHome.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityHome.js.vm
 */
import React from 'react';

import * as CourseService from './services/CourseService';
import * as PeriodService from './services/PeriodService';
import * as TeacherService from './services/TeacherService';

import settings from './services/settings';

import {
    HomeHeader
}
from './components/PageHeader';

import CourseList from './CourseList';
import CourseFormWindow from './CourseFormWindow';


export default React.createClass({

            getInitialState() {
                    return {
                        courses: [],
                        ,
                        periods: [],
                        periodId: settings.currentPeriod,
                        teachers: [],
                        teacherId: settings.currentTeacher
                    };
                },

                componentDidMount() {
                    PeriodService.findAll().then(periods => this.setState({
                        periods
                    }));

                    StringService.findAll().then(periods => this.setState({
                        periods
                    }));
                    CourseService.findAll({
                        periodId: this.state.periodId
                    }).then(courses => this.setState({
                        courses
                    }));
                    StringService.findAll().then(teachers => this.setState({
                        teachers
                    }));
                    CourseService.findAll({
                        teacherId: this.state.teacherId
                    }).then(courses => this.setState({
                        courses
                    }));
                },

                editHandler(data) {
                    window.location.hash = "#course/" + data.id + "/edit";
                },

                viewChangeHandler(index, periodId, label) {
                    CourseService.findAll({
                        periodId
                    }).then(courses => this.setState({
                        periodId, courses
                    }));
                },

                newHandler() {
                    this.setState({
                        addingCourse: true
                    });
                },

                savedHandler(student) {
                    this.setState({
                        addingCourse: false
                    });
                    window.location.hash = "#course/" + course.id;
                },

                cancelHandler() {
                    this.setState({
                        addingCourse: false
                    });
                },

                render() {
                    return ( < div >
                        < HomeHeader type = "Courses"
                        title = "Current Courses"
                        newLabel = "New Course"
                        actions = {
                            [{
                                value: "new",
                                label: "New Course"
                            }]
                        }
                        itemCount = {
                            this.state.courses.length
                        }
                        viewperiodId = {
                            this.state.periodId
                        }
                        viewsperiod = {
                            this.state.periods
                        }
                        viewteacherId = {
                            this.state.teacherId
                        }
                        viewsteacher = {
                            this.state.teachers
                        }
                        onViewChange = {
                            this.viewChangeHandler
                        }
                        onNew = {
                            this.newHandler
                        }
                        /> < CourseList courses = {
                            this.state.courses
                        }
                        /> {
                            this.state.addingCourse ? < CourseFormWindow onSaved = {
                                this.savedHandler
                            }
                            onCancel = {
                                this.cancelHandler
                            }
                            />:null} < /div>
                        );
                    }

                });

        /* 
        [TRIVIA]
        It would take a person typing  @ 100.0 cpm, 
        approximately 24.99 minutes to type the 2499+ characters in this file.
         */