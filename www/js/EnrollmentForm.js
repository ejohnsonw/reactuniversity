/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     EnrollmentForm.js
Description:  Captures information for an entity
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityForm.js.vm
 */
import React from 'react/addons';

import * as EnrollmentService from './services/EnrollmentService';
import * as CourseService from './services/CourseService';
import * as StudentService from './services/StudentService';

import ComboBox from './components/ComboBox';

export default React.createClass({

    getInitialState() {
            return {
                enrollment: {},
                ,
                courses: [],
                students: []
            };
        },

        componentWillReceiveProps(props) {
            let enrollment = props.enrollment;
            this.setState({
                enrollment
            });
        },

        componentDidMount() {
            StringService.findAll().then(course => this.setState({
                courses
            }));
            StringService.findAll().then(student => this.setState({
                students
            }));
        },

        courseChangeHandler(index, value, label) {
            let enrollment = this.state.enrollment;
            enrollment.course.id = value;
            this.setState({
                enrollment
            });
        }
    idChangeHandler(event) {
        let enrollment = this.state.enrollment;
        enrollment.id = event.target.value;
        this.setState({
            enrollment
        });
    }, studentChangeHandler(index, value, label) {
        let enrollment = this.state.enrollment;
        enrollment.student.id = value;
        this.setState({
            enrollment
        });
    }

    save() {
            let saveItem = this.state.enrollment.id ? EnrollmentService.updateItem : EnrollmentService.createItem;
            saveItem(this.state.enrollment).then(savedEnrollment => {
                if (this.props.onSaved) this.props.onSaved(savedEnrollment);
            });
        },

        render() {
            let enrollment = this.state.enrollment;
            return ( < div className = "slds-form--stacked slds-grid slds-wrap" >
                < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2" >
                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Course < /label> < div className = "slds-form-element__control" >
                < ComboBox data = {
                    this.state.courses
                }
                value = {
                    enrollment.course
                }
                onChange = {
                    this.courseChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Id < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    enrollment.id
                }
                onChange = {
                    this.idChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Student < /label> < div className = "slds-form-element__control" >
                < ComboBox data = {
                    this.state.students
                }
                value = {
                    enrollment.student
                }
                onChange = {
                    this.studentChangeHandler
                }
                /> < /div> < /div> < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 33.91 minutes to type the 3391+ characters in this file.
 */