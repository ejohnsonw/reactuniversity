/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     StudentForm.js
Description:  Captures information for an entity
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityForm.js.vm
 */
import React from 'react/addons';

import * as StudentService from './services/StudentService';

import ComboBox from './components/ComboBox';

export default React.createClass({

    getInitialState() {
            return {
                student: {},
            };
        },

        componentWillReceiveProps(props) {
            let student = props.student;
            this.setState({
                student
            });
        },

        componentDidMount() {},

        addressChangeHandler(event) {
            let student = this.state.student;
            student.address = event.target.value;
            this.setState({
                student
            });
        }
    cityChangeHandler(event) {
        let student = this.state.student;
        student.city = event.target.value;
        this.setState({
            student
        });
    }, dobChangeHandler(event) {
        let student = this.state.student;
        student.dob = event.target.value;
        this.setState({
            student
        });
    }, emailChangeHandler(event) {
        let student = this.state.student;
        student.email = event.target.value;
        this.setState({
            student
        });
    }, enrollmentsChangeHandler(index, value, label) {
        // Should add to collection
        /*let student = this.state.student;
        student.enrollments.id = value;
        this.setState({student});*/
    }, firstNameChangeHandler(event) {
        let student = this.state.student;
        student.firstName = event.target.value;
        this.setState({
            student
        });
    }, idChangeHandler(event) {
        let student = this.state.student;
        student.id = event.target.value;
        this.setState({
            student
        });
    }, lastNameChangeHandler(event) {
        let student = this.state.student;
        student.lastName = event.target.value;
        this.setState({
            student
        });
    }, lastupdateChangeHandler(event) {
        let student = this.state.student;
        student.lastupdate = event.target.value;
        this.setState({
            student
        });
    }, mobilePhoneChangeHandler(event) {
        let student = this.state.student;
        student.mobilePhone = event.target.value;
        this.setState({
            student
        });
    }, phoneChangeHandler(event) {
        let student = this.state.student;
        student.phone = event.target.value;
        this.setState({
            student
        });
    }, picChangeHandler(event) {
        let student = this.state.student;
        student.pic = event.target.value;
        this.setState({
            student
        });
    }, registrationChangeHandler(event) {
        let student = this.state.student;
        student.registration = event.target.value;
        this.setState({
            student
        });
    }, stateChangeHandler(event) {
        let student = this.state.student;
        student.state = event.target.value;
        this.setState({
            student
        });
    }, zipChangeHandler(event) {
        let student = this.state.student;
        student.zip = event.target.value;
        this.setState({
            student
        });
    }

    save() {
            let saveItem = this.state.student.id ? StudentService.updateItem : StudentService.createItem;
            saveItem(this.state.student).then(savedStudent => {
                if (this.props.onSaved) this.props.onSaved(savedStudent);
            });
        },

        render() {
            let student = this.state.student;
            return ( < div className = "slds-form--stacked slds-grid slds-wrap" >
                < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2" >
                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Address < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.address
                }
                onChange = {
                    this.addressChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > City < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.city
                }
                onChange = {
                    this.cityChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Dob < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.dob
                }
                onChange = {
                    this.dobChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Email < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.email
                }
                onChange = {
                    this.emailChangeHandler
                }
                /> < /div> < /div>

                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > FirstName < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.firstName
                }
                onChange = {
                    this.firstNameChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Id < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.id
                }
                onChange = {
                    this.idChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > LastName < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.lastName
                }
                onChange = {
                    this.lastNameChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Lastupdate < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.lastupdate
                }
                onChange = {
                    this.lastupdateChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > MobilePhone < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.mobilePhone
                }
                onChange = {
                    this.mobilePhoneChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Phone < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.phone
                }
                onChange = {
                    this.phoneChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Pic < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.pic
                }
                onChange = {
                    this.picChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Registration < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.registration
                }
                onChange = {
                    this.registrationChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > State < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.state
                }
                onChange = {
                    this.stateChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Zip < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    student.zip
                }
                onChange = {
                    this.zipChangeHandler
                }
                /> < /div> < /div> < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 103.12 minutes to type the 10312+ characters in this file.
 */