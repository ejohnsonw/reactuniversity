/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     EnrollmentList.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityList.js.vm
 */
import React from 'react';
import DataGrid from './components/DataGrid';

export default React.createClass({

    enrollmentLinkHandler(enrollment) {
            window.location.hash = "#enrollment/" + enrollment.id;
        },
        courseLinkHandler(enrollment) {
            window.location.hash = "#course/" + enrollment.course;
        },
        studentLinkHandler(enrollment) {
            window.location.hash = "#student/" + enrollment.student;
        },


        rowClick(data) {
            if (this.props.onRowClick) {
                this.props.onRowClick(data);
            }
        },

        render() {
            return ( < DataGrid data = {
                    this.props.enrollments
                }
                onRowClick = {
                    this.rowClick
                }
                ignoreLinks = {
                    this.props.ignoreLinks
                } >
                < /DataGrid>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 7.46 minutes to type the 746+ characters in this file.
 */