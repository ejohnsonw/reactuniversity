/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     Shell.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/shell.js.vmg
 */
import React from 'react';

import Spinner from './components/Spinner';
import Toast from './components/Toast';
import {
    Icon
}
from './components/Icons';

export default React.createClass({

    render() {
        return ( < div >
            < Spinner / >
            < Toast / >
            < header className = "menu"
            style = {
                {
                    backgroundColor: "#01344E",
                    verticalAlign: "middle"
                }
            } >
            < ul className = "slds-list--horizontal" >
            < li className = "slds-list__item" > < a href = "#periods" > < Icon name = "orders"
            theme = {
                null
            }
            />Periods</a > < /li> < li className = "slds-list__item" > < a href = "#teachers" > < Icon name = "orders"
            theme = {
                null
            }
            />Teachers</a > < /li> < li className = "slds-list__item" > < a href = "#courses" > < Icon name = "orders"
            theme = {
                null
            }
            />Courses</a > < /li> < li className = "slds-list__item" > < a href = "#students" > < Icon name = "orders"
            theme = {
                null
            }
            />Students</a > < /li> < li className = "slds-list__item" > < a href = "#enrollments" > < Icon name = "orders"
            theme = {
                null
            }
            />Enrollments</a > < /li> < /ul> < /header> {
                this.props.children
            } < /div>
        );
    }
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 12.24 minutes to type the 1224+ characters in this file.
 */