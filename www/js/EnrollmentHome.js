/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     EnrollmentHome.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityHome.js.vm
 */
import React from 'react';

import * as EnrollmentService from './services/EnrollmentService';
import * as CourseService from './services/CourseService';
import * as StudentService from './services/StudentService';

import settings from './services/settings';

import {
    HomeHeader
}
from './components/PageHeader';

import EnrollmentList from './EnrollmentList';
import EnrollmentFormWindow from './EnrollmentFormWindow';


export default React.createClass({

            getInitialState() {
                    return {
                        enrollments: [],
                        ,
                        courses: [],
                        courseId: settings.currentCourse,
                        students: [],
                        studentId: settings.currentStudent
                    };
                },

                componentDidMount() {
                    PeriodService.findAll().then(periods => this.setState({
                        periods
                    }));

                    StringService.findAll().then(courses => this.setState({
                        courses
                    }));
                    EnrollmentService.findAll({
                        courseId: this.state.courseId
                    }).then(enrollments => this.setState({
                        enrollments
                    }));
                    StringService.findAll().then(students => this.setState({
                        students
                    }));
                    EnrollmentService.findAll({
                        studentId: this.state.studentId
                    }).then(enrollments => this.setState({
                        enrollments
                    }));
                },

                editHandler(data) {
                    window.location.hash = "#enrollment/" + data.id + "/edit";
                },

                viewChangeHandler(index, periodId, label) {
                    EnrollmentService.findAll({
                        periodId
                    }).then(enrollments => this.setState({
                        periodId, enrollments
                    }));
                },

                newHandler() {
                    this.setState({
                        addingEnrollment: true
                    });
                },

                savedHandler(student) {
                    this.setState({
                        addingEnrollment: false
                    });
                    window.location.hash = "#enrollment/" + enrollment.id;
                },

                cancelHandler() {
                    this.setState({
                        addingEnrollment: false
                    });
                },

                render() {
                    return ( < div >
                        < HomeHeader type = "Enrollments"
                        title = "Current Enrollments"
                        newLabel = "New Enrollment"
                        actions = {
                            [{
                                value: "new",
                                label: "New Enrollment"
                            }]
                        }
                        itemCount = {
                            this.state.enrollments.length
                        }
                        viewcourseId = {
                            this.state.courseId
                        }
                        viewscourse = {
                            this.state.courses
                        }
                        viewstudentId = {
                            this.state.studentId
                        }
                        viewsstudent = {
                            this.state.students
                        }
                        onViewChange = {
                            this.viewChangeHandler
                        }
                        onNew = {
                            this.newHandler
                        }
                        /> < EnrollmentList enrollments = {
                            this.state.enrollments
                        }
                        /> {
                            this.state.addingEnrollment ? < EnrollmentFormWindow onSaved = {
                                this.savedHandler
                            }
                            onCancel = {
                                this.cancelHandler
                            }
                            />:null} < /div>
                        );
                    }

                });

        /* 
        [TRIVIA]
        It would take a person typing  @ 100.0 cpm, 
        approximately 26.27 minutes to type the 2627+ characters in this file.
         */