/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     PeriodRecord.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityRecord.js.vm
 */
import React from 'react';
import moment from 'moment';

import * as PeriodService from './services/PeriodService';

import {
    RecordHeader, HeaderField
}
from './components/PageHeader';

import PeriodView from './PeriodView';
import PeriodEnrollmentCard from './PeriodEnrollmentCard';

export default React.createClass({

    getInitialState() {
            return {
                period: {}
            };
        },

        componentDidMount() {
            this.getPeriod(this.props.params.periodId);
        },

        componentWillReceiveProps(props) {
            this.getPeriod(props.params.periodId);
        },

        getPeriod(id) {
            PeriodService.findById(id).then(period => this.setState({
                period
            }));
        },

        deleteHandler() {
            PeriodService.deleteItem(this.state.period.id).then(() => window.location.hash = "periods");
        },

        editHandler() {
            window.location.hash = "#period/" + this.state.period.id + "/edit";
        },

        render() {
            return ( < div >
                < RecordHeader type = "Period"
                icon = "orders"
                title = {
                    this.state.period.name
                }
                onEdit = {
                    this.editHandler
                }
                onDelete = {
                    this.deleteHandler
                } >
                < HeaderField label = "Name"
                value = {
                    this.state.period.name
                }
                /> < /RecordHeader>

                {
                    React.cloneElement(this.props.children, {
                        period: this.state.period
                    })
                }

                < /div>
            );
        }
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 14.37 minutes to type the 1437+ characters in this file.
 */