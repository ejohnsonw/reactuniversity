/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     CourseView.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityView.js.vm
 */
import React from 'react';

import CourseEnrollmentCard from "./CourseEnrollmentCard";

export default React.createClass({

    render() {
        let course = this.props.course;
        return ( < div className = "slds-m-around--medium" >
            < div className = "slds-grid slds-wrap slds-m-bottom--large" >
            < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2 slds-m-top--medium" >
            < dl className = "page-header--rec-home__detail-item" >
            < dt >
            < p className = "slds-text-heading--label slds-truncate"
            title = "Field 1" > Period < /p> < /dt> < dd >
            < p className = "slds-text-body--regular slds-truncate"
            title = "" > {
                course.name
            } < /p> < /dd> < /dl> < dl className = "page-header--rec-home__detail-item" >
            < dt >
            < p className = "slds-text-heading--label slds-truncate"
            title = "Field 1" > Teacher < /p> < /dt> < dd >
            < p className = "slds-text-body--regular slds-truncate"
            title = "" > {
                course.name
            } < /p> < /dd> < /dl>


            < /div> < /div> < CourseEnrollmentCard course = {
                course
            }
            /> < /div>
        );
    }
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 14.08 minutes to type the 1408+ characters in this file.
 */