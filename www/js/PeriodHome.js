/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     PeriodHome.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityHome.js.vm
 */
import React from 'react';

import * as PeriodService from './services/PeriodService';

import settings from './services/settings';

import {
    HomeHeader
}
from './components/PageHeader';

import PeriodList from './PeriodList';
import PeriodFormWindow from './PeriodFormWindow';


export default React.createClass({

            getInitialState() {
                    return {
                        periods: [],
                    };
                },

                componentDidMount() {
                    PeriodService.findAll().then(periods => this.setState({
                        periods
                    }));

                },

                editHandler(data) {
                    window.location.hash = "#period/" + data.id + "/edit";
                },

                viewChangeHandler(index, periodId, label) {
                    PeriodService.findAll({
                        periodId
                    }).then(periods => this.setState({
                        periodId, periods
                    }));
                },

                newHandler() {
                    this.setState({
                        addingPeriod: true
                    });
                },

                savedHandler(student) {
                    this.setState({
                        addingPeriod: false
                    });
                    window.location.hash = "#period/" + period.id;
                },

                cancelHandler() {
                    this.setState({
                        addingPeriod: false
                    });
                },

                render() {
                    return ( < div >
                        < HomeHeader type = "Periods"
                        title = "Current Periods"
                        newLabel = "New Period"
                        actions = {
                            [{
                                value: "new",
                                label: "New Period"
                            }]
                        }
                        itemCount = {
                            this.state.periods.length
                        }
                        onViewChange = {
                            this.viewChangeHandler
                        }
                        onNew = {
                            this.newHandler
                        }
                        /> < PeriodList periods = {
                            this.state.periods
                        }
                        /> {
                            this.state.addingPeriod ? < PeriodFormWindow onSaved = {
                                this.savedHandler
                            }
                            onCancel = {
                                this.cancelHandler
                            }
                            />:null} < /div>
                        );
                    }

                });

        /* 
        [TRIVIA]
        It would take a person typing  @ 100.0 cpm, 
        approximately 17.04 minutes to type the 1704+ characters in this file.
         */