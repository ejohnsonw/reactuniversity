/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     StudentRecord.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityRecord.js.vm
 */
import React from 'react';
import moment from 'moment';

import * as StudentService from './services/StudentService';

import {
    RecordHeader, HeaderField
}
from './components/PageHeader';

import StudentView from './StudentView';
import StudentEnrollmentCard from './StudentEnrollmentCard';

export default React.createClass({

    getInitialState() {
            return {
                student: {}
            };
        },

        componentDidMount() {
            this.getStudent(this.props.params.studentId);
        },

        componentWillReceiveProps(props) {
            this.getStudent(props.params.studentId);
        },

        getStudent(id) {
            StudentService.findById(id).then(student => this.setState({
                student
            }));
        },

        deleteHandler() {
            StudentService.deleteItem(this.state.student.id).then(() => window.location.hash = "students");
        },

        editHandler() {
            window.location.hash = "#student/" + this.state.student.id + "/edit";
        },

        render() {
            return ( < div >
                < RecordHeader type = "Student"
                icon = "orders"
                title = {
                    this.state.student.name
                }
                onEdit = {
                    this.editHandler
                }
                onDelete = {
                    this.deleteHandler
                } >
                < HeaderField label = "FirstName"
                value = {
                    this.state.student.firstName
                }
                /> < HeaderField label = "LastName"
                value = {
                    this.state.student.lastName
                }
                /> < /RecordHeader>

                {
                    React.cloneElement(this.props.children, {
                        student: this.state.student
                    })
                }

                < /div>
            );
        }
});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 15.430001 minutes to type the 1543+ characters in this file.
 */