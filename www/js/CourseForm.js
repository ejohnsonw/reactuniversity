/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     CourseForm.js
Description:  Captures information for an entity
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityForm.js.vm
 */
import React from 'react/addons';

import * as CourseService from './services/CourseService';
import * as PeriodService from './services/PeriodService';
import * as TeacherService from './services/TeacherService';

import ComboBox from './components/ComboBox';

export default React.createClass({

    getInitialState() {
            return {
                course: {},
                ,
                periods: [],
                teachers: []
            };
        },

        componentWillReceiveProps(props) {
            let course = props.course;
            this.setState({
                course
            });
        },

        componentDidMount() {
            StringService.findAll().then(period => this.setState({
                periods
            }));
            StringService.findAll().then(teacher => this.setState({
                teachers
            }));
        },

        codeChangeHandler(event) {
            let course = this.state.course;
            course.code = event.target.value;
            this.setState({
                course
            });
        }
    creditsChangeHandler(event) {
        let course = this.state.course;
        course.credits = event.target.value;
        this.setState({
            course
        });
    }, enrollmentsChangeHandler(index, value, label) {
        // Should add to collection
        /*let course = this.state.course;
        course.enrollments.id = value;
        this.setState({course});*/
    }, idChangeHandler(event) {
        let course = this.state.course;
        course.id = event.target.value;
        this.setState({
            course
        });
    }, nameChangeHandler(event) {
        let course = this.state.course;
        course.name = event.target.value;
        this.setState({
            course
        });
    }, periodChangeHandler(index, value, label) {
        let course = this.state.course;
        course.period.id = value;
        this.setState({
            course
        });
    }, teacherChangeHandler(index, value, label) {
        let course = this.state.course;
        course.teacher.id = value;
        this.setState({
            course
        });
    }

    save() {
            let saveItem = this.state.course.id ? CourseService.updateItem : CourseService.createItem;
            saveItem(this.state.course).then(savedCourse => {
                if (this.props.onSaved) this.props.onSaved(savedCourse);
            });
        },

        render() {
            let course = this.state.course;
            return ( < div className = "slds-form--stacked slds-grid slds-wrap" >
                < div className = "slds-col--padded slds-size--1-of-1 slds-medium-size--1-of-2" >
                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Code < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    course.code
                }
                onChange = {
                    this.codeChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Credits < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    course.credits
                }
                onChange = {
                    this.creditsChangeHandler
                }
                /> < /div> < /div>

                < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Id < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    course.id
                }
                onChange = {
                    this.idChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Name < /label> < div className = "slds-form-element__control" >
                < input className = "slds-input"
                type = "text"
                value = {
                    course.name
                }
                onChange = {
                    this.nameChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Period < /label> < div className = "slds-form-element__control" >
                < ComboBox data = {
                    this.state.periods
                }
                value = {
                    course.period
                }
                onChange = {
                    this.periodChangeHandler
                }
                /> < /div> < /div> < div className = "slds-form-element" >
                < label className = "slds-form-element__label"
                htmlFor = "sample1" > Teacher < /label> < div className = "slds-form-element__control" >
                < ComboBox data = {
                    this.state.teachers
                }
                value = {
                    course.teacher
                }
                onChange = {
                    this.teacherChangeHandler
                }
                /> < /div> < /div> < /div> < /div>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 55.25 minutes to type the 5525+ characters in this file.
 */