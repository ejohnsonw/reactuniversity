/* 
Copyright (c) 2015 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   Public Templates
Filename:     CourseList.js
Description:  Home
Project:      GeneratronUniversity
Template: /ReactSample/www/js/entityList.js.vm
 */
import React from 'react';
import DataGrid from './components/DataGrid';

export default React.createClass({

    courseLinkHandler(course) {
            window.location.hash = "#course/" + course.id;
        },
        periodLinkHandler(course) {
            window.location.hash = "#period/" + course.period;
        },
        teacherLinkHandler(course) {
            window.location.hash = "#teacher/" + course.teacher;
        },


        rowClick(data) {
            if (this.props.onRowClick) {
                this.props.onRowClick(data);
            }
        },

        render() {
            return ( < DataGrid data = {
                    this.props.courses
                }
                onRowClick = {
                    this.rowClick
                }
                ignoreLinks = {
                    this.props.ignoreLinks
                } >
                < div header = "Name"
                field = "Name"
                onLink = {
                    this.courseLinkHandler
                }
                /> < /DataGrid>
            );
        }

});

/* 
[TRIVIA]
It would take a person typing  @ 100.0 cpm, 
approximately 7.7899995 minutes to type the 779+ characters in this file.
 */